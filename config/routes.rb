Rails.application.routes.draw do
  get 'welcome/privacypol'
  get 'welcome/termcondition'
  get 'welcome/howto'
  get 'welcome/aboutus'
  get 'invitations/index'
  get 'invitations/recent'
  get 'sessions/new'
  get 'articles/new'
  get 'users/profil'
  get 'welcome/index'
  get 'events/recent'
  root 'welcome#index'
  resources :users 
  resources :events do
    member do
      get 'join'
      get 'leave'
    end
  end
  resources :invitations

  post 'invitations/index'
  get '/login' =>'sessions#new'
  get '/index' =>'welcome#index'
  get '/pp' => 'welcome#privacypol'
  get '/ht' => 'welcome#howto'
  get '/au' => 'welcome#aboutus'
  get '/tc' => 'welcome#termcondition'

  post '/login' =>'sessions#create'
  # post '/login' =>'sessions#save'
  get '/logout' =>'sessions#destroy'
  get '/signup' =>'users#new'
  post '/users'=>'users#create'
end
