class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :judul
      t.string :pemateri
      t.string :tag
      t.date :tgl
      t.time :waktu
      t.string :lokasi

      t.timestamps
    end
  end
end
