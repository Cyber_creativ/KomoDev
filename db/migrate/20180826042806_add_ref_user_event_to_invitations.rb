class AddRefUserEventToInvitations < ActiveRecord::Migration[5.2]
  def change
  	add_reference :invitations, :users, foreign_key: true
  	add_reference :invitations, :events, foreign_key: true
  end
end
