class Invitation < ApplicationRecord
  # has_and_belongs_to_many :users, dependent: :destroy
  # has_and_belongs_to_many :events, dependent: :destroy

  def self.check event_id, user_id
  	find_by(users_id: user_id, events_id: event_id).present?
  end
end
