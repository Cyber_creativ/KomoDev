class User < ApplicationRecord
# attr_accessor :image
# belongs_to :event
# has_many :events
# has_and_belongs_to_many :invitations, dependent: :destroy
mount_uploader :file, FileUploader
before_save { self.email = email.downcase}
validates :name, presence: true
VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
validates :email, presence: true, 
			format: {with: VALID_EMAIL_REGEX},
			uniqueness: {case_sensitive: false}
has_secure_password
end
