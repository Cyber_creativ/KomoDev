class InvitationsController < ApplicationController
  def index
    @events = Event.all
  	@invitations = Invitation.all
  end

  def recent
    @events = Event.all
    @invitations = Invitation.all
  end

  def show
    @event = Event.find(params[:id])
    @invitations = Invitation.all
    
  end
  
  def new
    @invitation = Invitation.new
  end



  def create
    @invitation = Invitation.new(invitation_params)
    
    if @invitation.save
      
    else
      render 'index'
    end
  end

  def edit
    @invitation = Invitation.find(params[:id])
  end

  def update
    @invitation = Invitation.find(params[:id])
    if @invitation.update(invitation_params) 
      redirect_to invitation_path(@invitation)
    else
      render 'edit'
    end 
  end

  # def destroy
  #   @invitation = Invitation.find(params[:id])
  #   @invitation.destroy


  # end
  def destroy
    @event = Event.find(params[:id])
    @event.destroy
 
    redirect_to event_path
  end

  private
    def invitation_params
      params.require(:invitation).permit(:events_id, :users_id)
    end
end
