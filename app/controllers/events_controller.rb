class EventsController < ApplicationController
  # before_action :authorize

def index
    @events = Event.all
    @users = User.all
  end
  def recent
    @events = Event.all
    @invitations = Invitation.all
  end

  def show
    @event = Event.find(params[:id])
    @users = User.all

  end
 
  def new
    @event = Event.new
  end
 
  def edit
    @event = Event.find(params[:id])
  end
 
  def create
    
      @event = Event.create(event_params)
      # @invitation = Invitation.create(invitation_params)
      if @event.save
        redirect_to @event
      else
        render 'new'
      end
    
      # if @invitation.save
        # redirect_to @invitation
        # render 'events'
      # else
        # render 'new'
      # end
    
  end

  def update
    @event = Event.find(params[:id])
    if @event.update(event_params) 
      redirect_to event_path(@event)
    else
      render 'edit'
    end 
  end
 
 def join
  @event = Event.find(params[:id])
    @event.update_attribute(:users_id, @users_id)
    redirect_to @event
    
    
end

def leave
  @event = Event.find params[:id]
  current_user.update_attribute(:event_id, nil)
  redirect_to @event
end

  def destroy
    @event = Event.find(params[:id])
    @event.destroy
 
    redirect_to events_path
  end
 
  private
    def event_params
      params.require(:event).permit(:judul, :pemateri, :tag, :tgl, :waktu, :lokasi, :files, :details, :users_id)
    end
end